import torch
import torchvision.transforms as transforms_test
import cv2
import numpy as np
from mmdet.apis import inference_detector, init_detector

from settings import DEVICE, THRESHOLD, NMS_THRESHOLD
from object_detection.predict import predict


class ImageAnalyser:

    def __init__(self, scenario_settings):
        self.scenario_settings = scenario_settings
        self.target_positions = self.scenario_settings.target_positions

        # self.model = torch.load(self.scenario_settings.PATH_TO_MODEL, map_location=DEVICE)
        self.model = init_detector(str(self.scenario_settings.path_to_model_config), 
                str(self.scenario_settings.path_to_model), DEVICE)

        self.results = []

    def reset_results(self):
        self.results = []

    def set_target_positions(self, target_positions):
        self.target_positions = target_positions

    def set_image(self, image):
        self.reset_results()
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        # image_tensor = transforms_test.ToTensor()(image)
        self.analyse(image)

    def analyse(self, image):
        centroids, labels = predict(image, self.model)
        for idx, label in enumerate(labels):
            grab_position = centroids[idx]
            print('centr and label: ', centroids[idx],labels[idx])
            # cv2.circle(image, (int(centroids[idx][0]), int(centroids[idx][1])),2,(0,255,0),2)

            grab_angle = 0.0
            drop_position = self.target_positions[label]
            self.results.append({'from': grab_position, 'to': drop_position, 'angle': grab_angle})
        # cv2.imwrite('test-banana.jpg', image)
        

    # @staticmethod
    # def get_grab_position(bounding_box):
    #     return [round((bounding_box[0]+bounding_box[2])/2), round((bounding_box[1]+bounding_box[3])/2)]

    # @staticmethod
    # def get_grab_angle(bounding_box):
    #     horizontal_length = abs(bounding_box[2] - bounding_box[0])
    #     vertical_length = abs(bounding_box[3] - bounding_box[1])
    #     if horizontal_length >= vertical_length:
    #         return 0.0
    #     else:
    #         return 0.5 * np.pi

    def get_results(self):
        return self.results
