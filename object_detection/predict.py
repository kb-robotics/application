import numpy as np
import cv2
import glob, os
from shapely.geometry import Polygon
from mmdet.apis import inference_detector, init_detector
import mmcv
from settings import CONF_SCORE, INTERSECTION_THRESHOLD


def mask2polygon(mask):
    polygons = []
    mask  = mask.astype(np.uint8)
    contours, _ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    contours = max(contours, key = cv2.contourArea)
    # print(contours)
    cnts = np.squeeze(contours)            
    if len(cnts.shape)==1:
        for c in cnts:
#              if the contour is small ignore it
            if  cv2.contourArea(np.array(c),False)>100: 
                polygon = Polygon(np.squeeze(c))
                polygons.append(polygon)
    else:
        polygon = Polygon(cnts)
        polygons.append(polygon)
    return polygons


def predict(image, model):
    CLASSES = model.CLASSES
    imgfr = image

    kernel = np.array([[0, -1, 0],
                    [-1, 5,-1],
                    [0, -1, 0]])
    image_sharp = cv2.filter2D(src=imgfr, ddepth=-1, kernel=kernel)
    result = inference_detector(model, image_sharp)
    segm_json_results = []
    data = None
    if isinstance(result, tuple):
        det, seg = result
        for label in range(len(det)):
            # bbox results
            bboxes = det[label]
            if isinstance(seg, tuple):
                segms = seg[0][label]
                mask_score = seg[1][label]
            else:
                segms = seg[label]
                mask_score = [bbox[4] for bbox in bboxes]
            for i in range(bboxes.shape[0]):
                if mask_score[i]>CONF_SCORE:
                    data = dict()
                    data['mask_score'] = float(mask_score[i])
                    data['category'] =  CLASSES[label]
                    data['segmentation'] = segms[i]
                    segm_json_results.append(data)

        # here is the nms on masks
        to_remove = []
        for p in range(len(segm_json_results)-2,-1,-1):
            t=p
            for q in range(len(segm_json_results)-1,(t),-1):
                mask1 = segm_json_results[p]['segmentation'].astype(np.uint8) 
                mask2 = segm_json_results[q]['segmentation'].astype(np.uint8) 
                score1 = segm_json_results[p]['mask_score']
                score2 = segm_json_results[q]['mask_score']
                p1 = mask2polygon(mask1)[0]
                p2 = mask2polygon(mask2)[0]
                intersect = p1.intersection(p2).area
                union = p1.union(p2).area
                iou = intersect / union
                if p1.covers(p2):
                    to_remove.append(q)
                if p2.covers(p1):
                    to_remove.append(p)

                if  iou>INTERSECTION_THRESHOLD:
                    if score1>score2:
                        to_remove.append(q)
                    else:
                        to_remove.append(p)

        to_remove = list(dict.fromkeys(to_remove))
        for ele in sorted(to_remove, reverse = True):
            del segm_json_results[ele]

        if len(segm_json_results)>0:    
            for i in range(len(segm_json_results)):
                mask_obj  = segm_json_results[i]['segmentation'].astype(np.uint8)
                polygon_obj = mask2polygon(mask_obj)[0]
                segm_json_results[i]['centroid'] = (polygon_obj.centroid.coords.xy[0][0], polygon_obj.centroid.coords.xy[1][0])
    centroids = [segm_json_results[i]['centroid'] for i in range(len(segm_json_results))]
    classess = [segm_json_results[i]['category'] for i in range(len(segm_json_results))]            
    return centroids,  classess


# predict(IMAGE_NAME,dir, model_name, device, conf_score, intersection_th)
