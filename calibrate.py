# Modification of code from visual-pushing-grasping by Andy Zeng:
# https://github.com/andyzeng/visual-pushing-grasping

# Run this script to calibrate the UR5 robot. Important settings to consider:
#   - CALIBRATION_COLLECT_DATA (boolean): parameter that specifies whether data should be collected to use in the calibration.
#           If set to False, the data of a previous calibration can be reused if it is still in the CALIBRATION_DATA_DIR folder.
#           If set to True, the robot needs to be connected in order to execute the movement commands required for calibrating.
#   - CALIBRATION_TOOL_ORIENTATION (list of length 3): the orientation of the tool (gripper) during calibration.
#   - CHESSBOARD_OFFSET_FROM_TOOL (list of length 3): the vector distance between the position of the end-effector and
#           the center of the chessboard used for calibration.
#   - CALIBRATION_GRID_STEP (float): Step size in meters between different positions for the robot to move to for calibration.
#   - CHECKERBOARD_SIZE (tuple of size 2): The dimension of the chessboard used for calibration. NOTE: the dimension refers
#           to the number of intersections between the black-and-white squares, not to the number of squares itself (there
#           is a difference of 1 along both axes). You can send your complaints to openCV about this.
#   - CALIBRATION_DATA_DIR: The directory where the collected data and the computed results will be stored.


import numpy as np
from scipy import optimize
from object_manipulation_module.calibration.data_collector import DataCollector
from object_manipulation_module.camera.camera import Camera
from object_manipulation_module.settings import CALIBRATION_COLLECT_DATA, CALIBRATION_DATA_DIR, CHESSBOARD_OFFSET_FROM_TOOL


if __name__ == '__main__':
    camera = Camera()

    if not CALIBRATION_DATA_DIR.exists():
        CALIBRATION_DATA_DIR.mkdir()

    if CALIBRATION_COLLECT_DATA:
        data_collector = DataCollector(camera)
        data_collector.run_data_collection()

    measured_pts = np.loadtxt(str(CALIBRATION_DATA_DIR / 'measured_pts.txt'), delimiter=' ')
    observed_pts = np.loadtxt(str(CALIBRATION_DATA_DIR / 'observed_pts.txt'), delimiter=' ')
    measured_pts = np.loadtxt(str(CALIBRATION_DATA_DIR / 'observed_pix.txt'), delimiter=' ')
    
    world2camera = np.eye(4)

    for idx in range(measured_pts.shape[0]):
        measured_pts[idx,:] += CHESSBOARD_OFFSET_FROM_TOOL

    # Estimate rigid transform with SVD (from Nghia Ho)
    def get_rigid_transform(A, B):
        assert len(A) == len(B)
        N = A.shape[0]  # Total points
        centroid_A = np.mean(A, axis=0)
        centroid_B = np.mean(B, axis=0)
        AA = A - np.tile(centroid_A, (N, 1))  # Centre the points
        BB = B - np.tile(centroid_B, (N, 1))
        H = np.dot(np.transpose(AA), BB) # Dot is matrix multiplication for array
        U, S, Vt = np.linalg.svd(H)
        R = np.dot(Vt.T, U.T)
        if np.linalg.det(R) < 0:  # Special reflection case
           Vt[2,:] *= -1
           R = np.dot(Vt.T, U.T)
        t = np.dot(-R, centroid_A.T) + centroid_B.T
        return R, t

    def get_rigid_transform_error(z_scale):
        global measured_pts, observed_pts, observed_pix, world2camera, camera

        # Apply z offset and compute new observed points using camera intrinsics
        observed_z = observed_pts[:,2:] * z_scale
        observed_x = np.multiply(observed_pix[:,[0]]-camera.intrinsics[0][2],observed_z/camera.intrinsics[0][0])
        observed_y = np.multiply(observed_pix[:,[1]]-camera.intrinsics[1][2],observed_z/camera.intrinsics[1][1])
        new_observed_pts = np.concatenate((observed_x, observed_y, observed_z), axis=1)

        # Estimate rigid transform between measured points and new observed points
        R, t = get_rigid_transform(np.asarray(measured_pts), np.asarray(new_observed_pts))
        t.shape = (3,1)
        world2camera = np.concatenate((np.concatenate((R, t), axis=1),np.array([[0, 0, 0, 1]])), axis=0)

        # Compute rigid transform error
        registered_pts = np.dot(R,np.transpose(measured_pts)) + np.tile(t,(1,measured_pts.shape[0]))
        error = np.transpose(registered_pts) - new_observed_pts
        error = np.sum(np.multiply(error,error))
        rmse = np.sqrt(error/measured_pts.shape[0])
        return rmse

    # Optimize z scale w.r.t. rigid transform error
    print('Calibrating...')
    z_scale_init = 1
    optim_result = optimize.minimize(get_rigid_transform_error, 
            np.asarray(z_scale_init), method='Nelder-Mead')
    camera_depth_offset = optim_result.x

    # Save camera optimized offset and camera pose
    print('Saving...')

    np.savetxt(str(CALIBRATION_DATA_DIR / 'camera_depth_scale.txt'), 
            camera_depth_offset, delimiter=' ')
    
    get_rigid_transform_error(camera_depth_offset)
    camera_pose = np.linalg.inv(world2camera)
    np.savetxt(CALIBRATION_DATA_DIR / 'camera_pose.txt', 
            camera_pose, delimiter=' ')
    
    print('Done.')
