# Modification of code from visual-pushing-grasping by Andy Zeng:
# https://github.com/andyzeng/visual-pushing-grasping


import cv2
from object_manipulation_module.robot_interface import RobotInterface


robot_interface = RobotInterface()


def initial_callback(event, x, y, flags, param):
    if event == cv2.EVENT_LBUTTONDOWN:
        print('please select callback')


def target_callback(event, x, y, flags, param):
    if event == cv2.EVENT_LBUTTONDOWN:
        print('set target position!')
        robot_interface.set_target_position(x, y)


def object_callback(event, x, y, flags, param):
    if event == cv2.EVENT_LBUTTONDOWN:
        print('moving object...')
        robot_interface.move_object(x, y)


def main():
    # Show color image from camera
    cv2.namedWindow('color')
    cv2.setMouseCallback('color', initial_callback)

    while True:
        camera_image = robot_interface.get_camera_image()
        bgr_data = cv2.cvtColor(camera_image, cv2.COLOR_RGB2BGR)

        cv2.imshow('color', bgr_data)
        key = cv2.waitKey(1)

        if key == ord('c'):
            break

        if key == ord('b'):
            print('you can now click on the position of the box')
            cv2.setMouseCallback('color', target_callback)

        if key == ord('o'):
            print('you can now click on the position of an object')
            cv2.setMouseCallback('color', object_callback)

    cv2.destroyAllWindows()


if __name__ == '__main__':
    main()
