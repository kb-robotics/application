import numpy as np
from object_manipulation_module.robot_interface_module.robot_controller import RobotController


# The KeyboardController class provides a user-friendly way to interact with a UR5 robot through the RobotController
# python class. Run to file to start the keyboard controller, and press 'h' + <enter> to print all the possible
# keyboard commands to control the UR5 robot. Note that after each command the <enter> key needs to be pressed in order
# to execute the command.

class KeyboardController:

    def __init__(self):
        """
        Initialize the class parameters, and define the map between keyboard commands and class methods to be executed
        given a keyboard command.
        """
        self.robot_controller = RobotController()
        self.key_map = {'h': self.print_help_msg,
                        'i': self.print_state,
                        'q': self.stop_robot,
                        'test': self.test_msg,
                        'upright': self.move_upright,
                        'home': self.move_home,
                        'd': self.move_pos_x,
                        'a': self.move_neg_x,
                        'w': self.move_pos_y,
                        's': self.move_neg_y,
                        'r': self.move_up,
                        'f': self.move_down,
                        'o': self.open_gripper,
                        'c': self.close_gripper,
                        'O': self.grip,
                        'C': self.release_grip,
                        '1': self.move_joint_0,
                        '2': self.move_joint_1,
                        '3': self.move_joint_2,
                        '4': self.move_joint_3,
                        '5': self.move_joint_4,
                        '6': self.move_joint_5,
                        'exit': self.exit}

    def run(self):
        """
        Start the loop that listens for keyboard commands and executes the corresponding robot commands.
        """
        print('\n' + '=' * 80)
        print('Welcome to the keyboard controller!')
        print('Type "h" and press <enter> for a list of possible keyboard commands.\n')
        while True:
            input_key = input('  >> ')
            if input_key == '':
                continue

            if input_key in self.key_map:
                fnc = self.key_map[input_key]
                should_exit = fnc()
                if should_exit:
                    print('\n' + '=' * 80 + '\n')
                    break
            else:
                print('{} is not a valid keyboard command.'.format(input_key))

    @staticmethod
    def print_help_msg():
        """
        Print all the possible keyboard commands to the terminal.
        """
        print('\n' + '='*80)
        print('h           - help, displays this list of possible keyboard commands')
        print('i           - info, print state of the robot')
        print('q           - quit, stop the movement of the robot')
        print('test        - sends a text message to the robot log to test the connection')
        print('upright     - put robot in upright position')
        print('home        - put robot in home position')
        print('d/a         - pos/neg x, control the x axis of the robot in steps of 50 mm')
        print('w/s         - pos/neg y, control the y axis of the robot in steps of 50 mm')
        print('r/f         - up/down, control the z axis of the robot in steps of 50 mm')
        print('o/c         - open/close gripper')
        print('O/C         - grip/release grip, this includes a small z-axis correction')
        print('1/2/3/4/5/6 - move respective joints by 1/16 of a full rotation')
        print('exit        - exit the keyboard controller')
        print('='*80 + '\n')
        return False

    def print_state(self):
        """
        Retrieve the current state of the robot using the RobotController class and print it to the terminal.
        """
        robot_state = self.robot_controller.get_state()
        pose = robot_state['pose']
        joints = robot_state['joints']

        print('\n' + '='*80)
        print('info, pose:')
        print('x  [{:6.2f}] y  [{:6.2f}] z  [{:6.2f}] in m\
             \nrx [{:6.2f}] ry [{:6.2f}] rz [{:6.2f}] in rad'.format(pose['x'], pose['y'], pose['z'],
                                                                     pose['rx'], pose['ry'], pose['rz']))
        print('\njoint angles')
        print('1 [{:6.2f}] 2 [{:6.2f}] 3 [{:6.2f}]\
             \n4 [{:6.2f}] 5 [{:6.2f}] 6 [{:6.2f}]'.format(joints[0], joints[1], joints[2],
                                                           joints[3], joints[4], joints[5]))
        print('='*80 + '\n')
        return False

    def stop_robot(self):
        """
        Stop the movement currently being executed by the robot.
        """
        self.robot_controller.stop_robot()
        return False

    def test_msg(self):
        """
        Send a fixed text message to the UR5 PolyScope's log.
        """
        message = 'Hello World!'
        self.robot_controller.send_text_msg(message)
        return False

    def move_upright(self):
        """
        Move the robot to an upright position
        """
        self.robot_controller.move_upright()
        return False

    def move_home(self):
        """
        Move the robot to a home position
        """
        self.robot_controller.move_home()
        return False

    def move_pos_x(self):
        """
        Move the robot's end-effector 50mm in the positive x direction
        """
        self.robot_controller.move_x(0.05)
        return False

    def move_neg_x(self):
        """
        Move the robot's end-effector 50mm in the negative x direction.
        """
        self.robot_controller.move_x(-0.05)
        return False

    def move_pos_y(self):
        """
        Move the robot's end-effector 50mm in the positive y direction
        """
        self.robot_controller.move_y(0.05)
        return False

    def move_neg_y(self):
        """
        Move the robot's end-effector 50mm in the negative y direction.
        """
        self.robot_controller.move_y(-0.05)
        return False

    def move_up(self):
        """
        Move the robot's end-effector 50mm in the positive z direction
        """
        self.robot_controller.move_z(0.05)
        return False

    def move_down(self):
        """
        Move the robot's end-effector 50mm in the negative z direction.
        """
        self.robot_controller.move_z(-0.05)
        return False

    def open_gripper(self):
        """
        Open the gripper of the robot
        """
        self.robot_controller.open_gripper()
        return False

    def close_gripper(self):
        """
        Close the gripper of the robot
        """
        self.robot_controller.close_gripper()
        return False

    def grip(self):
        """
        Close the gripper while moving the gripper a little upwards, in order to keep the 'fingertips' of the gripper
        at approximately the same height while the gripper is closing.
        """
        self.robot_controller.grip()
        return False

    def release_grip(self):
        """
        Open the gripper while moving the gripper a little downwards, in order to keep the 'fingertips' of the gripper
        at approximately the same height while the gripper is opening.
        """
        self.robot_controller.release_grip()
        return False

    def move_joint_0(self):
        """
        Rotate the joint of the robot with index 0 for 1/16th of a full rotation in the positive direction.
        """
        self.robot_controller.move_joint(0, np.pi/8)
        return False

    def move_joint_1(self):
        """
        Rotate the joint of the robot with index 1 for 1/16th of a full rotation in the positive direction.
        """
        self.robot_controller.move_joint(1, np.pi/8)
        return False

    def move_joint_2(self):
        """
        Rotate the joint of the robot with index 2 for 1/16th of a full rotation in the positive direction.
        """
        self.robot_controller.move_joint(2, np.pi/8)
        return False

    def move_joint_3(self):
        """
        Rotate the joint of the robot with index 3 for 1/16th of a full rotation in the positive direction.
        """
        self.robot_controller.move_joint(3, np.pi/8)
        return False

    def move_joint_4(self):
        """
        Rotate the joint of the robot with index 4 for 1/16th of a full rotation in the positive direction.
        """
        self.robot_controller.move_joint(4, np.pi/8)
        return False

    def move_joint_5(self):
        """
        Rotate the joint of the robot with index 5 for 1/16th of a full rotation in the positive direction.
        """
        self.robot_controller.move_joint(5, np.pi/8)
        return False

    def exit(self):
        """
        Do nothing and return True so that the loop is exited
        """
        return True


if __name__ == '__main__':
    keyboard_controller = KeyboardController()
    keyboard_controller.run()
