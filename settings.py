from pathlib import Path
import json
import torch


BASE_DIR = Path(__file__).resolve().parent

DEVICE = torch.device('cpu')
#device = "cuda:0"
THRESHOLD = 0.56
NMS_THRESHOLD = 0.3
CONF_SCORE = 0.75
INTERSECTION_THRESHOLD = 0.5

MODELS_DIR = BASE_DIR / 'object_detection' / 'models'
SCENARIOS_DIR = BASE_DIR / 'scenarios'


##################
#   Scenario 1   #
##################
# todo


##################
#   Scenario 2   #
##################
class Scenario2:
    path_to_model = MODELS_DIR / 'scenario-2.v1.pth'
    path_to_model_config = MODELS_DIR / 'scenario-2.v1.py'
    path_to_scenario = SCENARIOS_DIR / 'scenario2.json'

    class_mapping = {1: 'ripe', 2: 'unripe'}
    target_positions = json.loads(path_to_scenario.read_text())


##################
#   Scenario 3   #
##################
# todo


##################
#   Scenario 4   #
##################
# todo


##################
#   Scenario 5   #
##################
# todo
