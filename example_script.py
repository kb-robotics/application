import time
from object_manipulation_module.robot_interface import RobotInterface
from object_detection.image_analyzer import ImageAnalyser
from settings import Scenario2


DRY_RUN = True


def main():
    robot_interface = RobotInterface()
    image_analyzer = ImageAnalyser(Scenario2)

    robot_interface.go_to_upright_position()

    camera_image = robot_interface.get_camera_image()
    image_analyzer.set_image(camera_image)
    results = image_analyzer.get_results()

    while len(results) > 0:
        print('\nDetected {} objects'.format(len(results)))
        fruit = results[0]

        print('Setting target to:  {:d} {:d}'.format(fruit['to'][0], fruit['to'][1]))
        if not DRY_RUN:
            robot_interface.set_target_position(fruit['to'][0], fruit['to'][1])
        time.sleep(1)

        print('Moving object from: {:d} {:d}, with angle: {:4.2f}'.format(fruit['from'][0], fruit['from'][1], fruit['angle']))
        if not DRY_RUN:
            robot_interface.move_object(fruit['from'][0], fruit['from'][0], fruit['angle'])

        robot_interface.go_to_upright_position()
        camera_image = robot_interface.get_camera_image()
        image_analyzer.set_image(camera_image)
        results = image_analyzer.get_results()


if __name__ == '__main__':
    main()
